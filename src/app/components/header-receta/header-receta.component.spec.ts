import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderRecetaComponent } from './header-receta.component';

describe('HeaderRecetaComponent', () => {
  let component: HeaderRecetaComponent;
  let fixture: ComponentFixture<HeaderRecetaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HeaderRecetaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderRecetaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
