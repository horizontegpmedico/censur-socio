import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NavBarCensurComponent } from './nav-bar-censur.component';

describe('NavBarCensurComponent', () => {
  let component: NavBarCensurComponent;
  let fixture: ComponentFixture<NavBarCensurComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NavBarCensurComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NavBarCensurComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
