export interface FichaInfo {
    nombrePaciente: String;
    apellidoPaterno: String;
    apellidoMaterno: String;
    curp: String;
    edad: Number;
    genero: String;
    _id:String;
    callePaciente: String;
    fechaNacimientoPaciente:Date;
    estadoPaciente: String;
    paisPaciente: String;
    telefono: String;
    contactoEmergencia1Nombre: String;
    contactoEmergencia1Edad:String;
    contactoEmergencia1Telefono:String;
    tipoDeSangre:string;
    religion:string;
    sede:string
} 