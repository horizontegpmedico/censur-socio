import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PaquetePacienteComponent } from './paquete-paciente.component';

describe('PaquetePacienteComponent', () => {
  let component: PaquetePacienteComponent;
  let fixture: ComponentFixture<PaquetePacienteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PaquetePacienteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PaquetePacienteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
