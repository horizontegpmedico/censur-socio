import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BitacoraReaccionesAdversasComponent } from './bitacora-reacciones-adversas.component';

describe('BitacoraReaccionesAdversasComponent', () => {
  let component: BitacoraReaccionesAdversasComponent;
  let fixture: ComponentFixture<BitacoraReaccionesAdversasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BitacoraReaccionesAdversasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BitacoraReaccionesAdversasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
