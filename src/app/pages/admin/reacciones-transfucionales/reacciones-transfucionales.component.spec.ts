import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReaccionesTransfucionalesComponent } from './reacciones-transfucionales.component';

describe('ReaccionesTransfucionalesComponent', () => {
  let component: ReaccionesTransfucionalesComponent;
  let fixture: ComponentFixture<ReaccionesTransfucionalesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReaccionesTransfucionalesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReaccionesTransfucionalesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
