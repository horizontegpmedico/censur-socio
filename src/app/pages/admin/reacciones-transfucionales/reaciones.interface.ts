export interface Reacciones {
    status: string
    fecha: string
    paciente: Paciente
    proceso_censur: string
    reaccionestransfucionales: any[]
    tiparycruzar: any[]
    tipo_producto: string
    ventaCensur: any[]
  }
  
  export interface Paciente {
    nombrePaciente: string
    edad: string
    genero: string
    apellidoPaterno: string
    apellidoMaterno: string
    telefono: string
  }
  