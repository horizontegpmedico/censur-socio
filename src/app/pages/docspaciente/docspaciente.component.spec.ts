import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DocspacienteComponent } from './docspaciente.component';

describe('DocspacienteComponent', () => {
  let component: DocspacienteComponent;
  let fixture: ComponentFixture<DocspacienteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DocspacienteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DocspacienteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
