import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BitacoraConsultasComponent } from './bitacora-consultas.component';

describe('BitacoraConsultasComponent', () => {
  let component: BitacoraConsultasComponent;
  let fixture: ComponentFixture<BitacoraConsultasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BitacoraConsultasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BitacoraConsultasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
