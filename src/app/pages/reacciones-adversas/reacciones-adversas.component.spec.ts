import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReaccionesAdversasComponent } from './reacciones-adversas.component';

describe('ReaccionesAdversasComponent', () => {
  let component: ReaccionesAdversasComponent;
  let fixture: ComponentFixture<ReaccionesAdversasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReaccionesAdversasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReaccionesAdversasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
