import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BitacoraUltrasonidoComponent } from './bitacora-ultrasonido.component';

describe('BitacoraUltrasonidoComponent', () => {
  let component: BitacoraUltrasonidoComponent;
  let fixture: ComponentFixture<BitacoraUltrasonidoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BitacoraUltrasonidoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BitacoraUltrasonidoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
