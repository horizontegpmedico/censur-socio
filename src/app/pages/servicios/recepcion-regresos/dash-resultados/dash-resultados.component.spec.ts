import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DashResultadosComponent } from './dash-resultados.component';

describe('DashResultadosComponent', () => {
  let component: DashResultadosComponent;
  let fixture: ComponentFixture<DashResultadosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DashResultadosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DashResultadosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
