import { TestBed } from '@angular/core/testing';

import { BancodesangreService } from './bancodesangre.service';

describe('BancodesangreService', () => {
  let service: BancodesangreService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BancodesangreService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
